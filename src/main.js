import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import VueLodash from 'vue-lodash'
import store from './plugins/vuex'

Vue.use(VueLodash)

import 'material-design-icons-iconfont/dist/material-design-icons.css' 
import 'vuetify/dist/vuetify.min.css'

Vue.config.productionTip = false

new Vue({
  vuetify,
  store,
  render: h => h(App)
}).$mount('#app')
