import Vue from 'vue'
import VueX from 'vuex'
import axios from 'axios';

Vue.use(VueX)

var blankCustomer = {
  customerName: '',
  serverName: '',
  isVisible: true,
  sslTimeOut: 60,
  locations: [
    {
      value: "",
      proxy_pass: ""
    }
  ]
}

export default new VueX.Store({
  state: {
    loading: false,
    confs: [],
    listIDs: []
  },
  mutations: {
    ADD_CUSTOMER(state, customer) {
      state.confs.push(customer)
    },
    REMOVE_CUSTOMER(state, customer) {
      state.confs.splice(state.confs.indexOf(customer), 1)
    },
    UPDATE_CUSTOMER(state, customer) {
      state.confs[state.confs.indexOf(customer)] = customer
    },
    SET_CUSTOMERS(state, customers) {
      state.confs = customers
      state.listIDs = []
      customers.forEach(element => {
        state.listIDs.push(element.serverName.match(/^(.*)\.(.*)\.(.*)$/i)[1])
      });
    },
    SET_LOADING_STATE(state, booleanLoading) {
      state.loading = booleanLoading
    },
    FILTER_CUSTOMERS(state, field) {
      var regex = new RegExp('.*' + field + '.*', 'i')
      state.confs.forEach(function (element) {
        element.isVisible = false
        if (
          element.customerName.match(regex) != null && element.customerName.match(regex).length > 0
          || field == null
          || field == ''
            ) {
          element.isVisible = true
        }
      })
    }
  },
  actions: {
    deleteOneCustomer: function (context, customer) {
      context.commit('REMOVE_CUSTOMER', customer)
    },
    updateOneCustomer: function (context, customer) {
      context.commit('UPDATE_CUSTOMER', customer)
    },
    addOneCustomer: function (context) {
      var newCustomer = JSON.parse(JSON.stringify(blankCustomer))
      do {
        var random = Math.floor(Math.random() * Math.floor(100000000)) 
      } while (context.state.listIDs.includes(random))

      newCustomer.serverName = random + '.flexmaint.com'
      context.commit('ADD_CUSTOMER', newCustomer)
    },
    triggerLoading: function (context) {
      context.commit('SET_LOADING_STATE', !context.state.loading)
    },
    searchCustomers: function (context, field) {
      context.commit('SET_LOADING_STATE', true)
      context.commit('FILTER_CUSTOMERS', field)
      context.commit('SET_LOADING_STATE', false)
    },
    fetchConf: function (context, file) {
      context.commit('SET_LOADING_STATE', true)
      let formData = new FormData()
      formData.append('fileConf', file)
      var protocol = location.protocol
      var slashes = protocol.concat("//")
      var host = slashes.concat(window.location.hostname)
      
      axios
        .post(host + ':8080/uploadfile',
        formData,
        {
          headers: {
            'Content-Type': 'multipart/form-data'
          },
          json: true
        })
        .then(function (response) {
          response.data.forEach(element => {
            element.isVisible = true
          });
          context.commit('SET_CUSTOMERS', response.data)
          context.commit('SET_LOADING_STATE', false)
        })
        .catch(function (response) {
          alert('Something went wrong on fetching data')
          context.commit('SET_LOADING_STATE', false)
        })
    }
  },
  getters: {
    filteredConfs: state => {
      // return state.confs.filter(conf => conf.isVisible)
      return state.confs
    }
  }
})
